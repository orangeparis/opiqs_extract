#!/usr/bin/python
# -*- coding: utf-8 -*-
import os.path
import subprocess


PORTAIL_BASE = "/opt/opiqs/"



class Interface():
    """This class is an interface between the API Server and the cluster hadoop.
        It will be used by the API Server to:
         * Launch the script for extraction
         * Retreive the URI to the file extracted
         * Retreive the status of the extraction
    """
    default_sources_list = ("tvdc", "ktrack")
    default_conf_filename = 'extract.conf'

    @classmethod
    def set_workspace ( self, workspace, path_conf ):
        """
        Write a conf file containing the workspace path. Ths config file is written in the directory path_conf defined ;
        In case the directory do not exists it is created

        :param workspace: path to the workspace
        :return: true if Ok set and false if an error occured
        """
        logger = logging.getLogger("root.interface.set_workspace")

        if not os.path.isdir(workspace):
            logger.error("The dir '%s' is not a valid directory" % workspace)
            return False

        try:
            os.makedirs(path_conf)
        except OSError, e:
            if e.errno == 17:
                pass
            else:
                print "An error occured : %s" % e
                return False

        with open(os.path.join(path_conf, Interface.default_conf_filename), "w") as f:
            f.write(workspace)

        return True

    @classmethod
    def get_workspace ( self,path_conf ):
        """
        Read the conf file containing the workspace path. Ths config file is read from the directory path_conf defined

        :return: workspace paht of None in case of error
        """
        logger = logging.getLogger("root.interface.get_workspace")

        try:
            with open(os.path.join(path_conf, Interface.default_conf_filename), "r") as f:
                workspace = f.read()
        except:
            return PORTAIL_BASE + "workspace"

        return workspace

    @classmethod
    def extraction ( self, start_date, end_date, type_field, values, sources, path_script, path_destination ,dry_mode=False):
        """This method permit to run a pig script
        :param path_destination: Destination path to write the csv result
        :param path: Path containing all pig/hql scripts
        :type sources: IGNORED list
        :param sources: IGNORED List of sources to be used. Can be 'all' in this case the values in "default_sources_list" will be used
        :param values: which is a list  values of type defined by the type
        :param type_field: which can be either mac,nd,serial,fti,aid
        :param end_date: Ending period timestamp
        :param start_date: Beginning period timestamp
        :param script: Name of pig script

        :return: dict with the return code of the command and the output
        """
        logger = logging.getLogger("root.interface.extraction")

        if dry_mode:
            logger.warning("!!!!! YOU ARE IN DRY MODE !!!!")

        if "all" in sources:
            sources = Interface.default_sources_list

        logger.debug("Sources: %s" % sources)
        sources_list = {
            'tvdc': ('read_tvdc.hql', Interface.hive)
        }

        # Create the destination directory if not already exists
        try:
            os.makedirs(path_destination)
        except OSError, e:
            if e.errno == 17:
                pass
            else:
                raise OSError(e)

        for source in sources:
            (script_name, func) = sources_list[source]
            script = os.path.join(path_script, script_name)
            logger.debug("Script to launch: %s" % script)
            return func(start_date, end_date, type_field, values, script, path_destination,dry_mode=dry_mode)
        return None, None

    @classmethod
    def pig ( self, start_date, end_date, type_field, values, script, path_destination, executable = '/usr/bin/pig',
              engine = 'tez', dry_mode= False
              ):
        """This method permit to run a pig script
        :param path_destination: Destination path to write the csv result
        :param engine: Engine to be use to launch the script, either tez or mapreduce (by default tez)
        :param executable: Name of Pig interpreter (by default /usr/bin/pig)
        :param values: which is a list  values of type defined by the type
        :param type_field: which can be either mac,nd,serial,fti,aid
        :param end_date: Ending period timestamp
        :param start_date: Beginning period timestamp
        :param script: Name of pig script

        :return: dict with the return code of the command and the output
        """
        logger = logging.getLogger("root.interface.extraction.pig")
        # Convert a tuple to string with this format
        # (AA:BB:DD,EE:RR:TT,SS:YY:UU)
        # to
        # 'AA:BB:DD','EE:RR:TT','SS:YY:UU'
        formatted_values = ','.join(["'{}'".format(value) for value in values])

        # Add parameter -useHCatalog to read Hive Table
        # (https://www.forge.orange-labs.fr/plugins/mediawiki/wiki/opiqs/index.php/Sources/TVDC)
        args = ('-x', engine,
                '-useHCatalog',
                '-f', script,
                '-p', 'FIELD=%s' % type_field,
                '-p', u'VALUES="{0:s}"'.format(formatted_values),
                '-p', 'START_DATE=%d' % (start_date * 1000),
                '-p', 'END_DATE=%d' % (end_date * 1000),
                '-p', 'DEST=%s' % path_destination)

        if not dry_mode:
            # real execution
            return Interface.run_process(executable, args, cwd=path_destination)
        else:
            # run in dry mode, no action taken
            return Interface.run_dry(executable, args, cwd=path_destination)


    @classmethod
    def hive ( self, start_date, end_date, type_field, values, script, path_destination,
               executable = '/usr/bin/beeline', dry_mode= False):
        """This method permit to run a pig script
        :param path_destination: Destination path to write the csv result
        :type executable: str
        :param executable: Name of interpreter (by default /usr/bin/hive)
        :param values: which is a list  values of type defined by the type
        :param type_field: which can be either mac,nd,serial,fti,aid
        :param end_date: Ending period timestamp (in seconds)
        :param start_date: Beginning period timestamp (in seconds)
        :param script: Name of pig script

        :return: dict with the return code of the command and the output
        """
        logger = logging.getLogger("root.interface.extraction.hive")
        # Convert a tuple to string with this format
        # (AA:BB:DD,EE:RR:TT,SS:YY:UU) to 'AA:BB:DD','EE:RR:TT','SS:YY:UU'
        formatted_values = ','.join(["'{}'".format(value) for value in values])

        args = ('-u', 'jdbc:hive2://arthur:10000',
                '--color=true',
                '--hiveconf', 'FIELD=%s' % type_field,
                '--hiveconf', u'VALUES={0:s}'.format(formatted_values),
                '--hiveconf', 'START_DATE=%d' % start_date,
                '--hiveconf', 'END_DATE=%d' % end_date,
                '-f', script,
                '-n', 'qostv')

        if not dry_mode:
            # we launch command for real
            rc, output = Interface.run_process(executable, args,cwd=path_destination)
        else:
            # run in dry mode : no action taken
            rc, output = Interface.run_dry(executable, args,cwd=path_destination)

        # Write to destination file after removing "jdbc:hive" lines
        filename = os.path.join(path_destination, "%s.csv" % os.path.basename(script)[:-4])
        logger.info("Write result to file %s" % filename)
        with open(filename, "w") as result_file:
            for line in output.split('\n'):
                if line[:13] != "0: jdbc:hive2":
                    result_file.write("%s\n" % line)
        return rc, output

    @classmethod
    def run_process ( self, executable, options,cwd=PORTAIL_BASE):
        """This method permit to run a pig script
        :param options: tuple containing all options passed to process to launch
        :param executable: name of process to launch

        :return: tuple with the return code of the command and the output
        """
        assert (type(options) is tuple)

        logger = logging.getLogger("root.interface.run_process")

        args = (executable,) + options

        logger.debug("Command line : %s" % (" ".join(args)))
        logger.debug("Command directory : %s" % cwd)
        return_code = 0
        result_process = ""
        try:
            result_process = subprocess.check_output(args,cwd=cwd)
        except subprocess.CalledProcessError, e:
            return_code = e
            pass

        return return_code, result_process


    @classmethod
    def run_dry ( self, executable, options,cwd=PORTAIL_BASE):
        """This method permit to run a pig script
        :param options: tuple containing all options passed to process to launch
        :param executable: name of process to launch

        :return: tuple with the return code of the command and the output
        """
        assert (type(options) is tuple)

        logger = logging.getLogger("root.interface.run_process")

        args = (executable,) + options

        logger.warning("!!!!!!! YOU ARE IN DRY MODE  no action taken  !!!!!!")
        logger.debug("Command line : %s" % (" ".join(args)))
        logger.debug("Command directory : %s" % cwd)
        return_code = 0
        result_process = ""

        # try:
        #     result_process = subprocess.check_output(args,cwd=cwd)
        # except subprocess.CalledProcessError, e:
        #     return_code = e
        #     pass
        return_code = 0
        result_process= "DRY MODE"

        return return_code, result_process



if __name__ == '__main__':
    import time
    import argparse
    import logging

    import sys

    # create logger
    logger = logging.getLogger("root.interface")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)

    # create formatter
    formatter_console = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter_console)

    # add ch to logger
    logger.addHandler(ch)

    logger.info("===Starting===")
    # Script menu
    parser = argparse.ArgumentParser(
            description = 'Script used as interface with API server. It is intendend to run PIG script')
    parser.add_argument('--debug', action = "store_true",
                        help = 'Write all debug log to console', required = False, default = False)
    parser.add_argument('--dry', action = "store_true",
                        help = 'dry mode: no action taken', required = False, default = False)
    parser.add_argument('--taskname', '-t', action = "store",
                        help = 'Task name, used to decide where to write the destination file to. '
                               'It is written in the workspace + taskname directory',
                        required = False, default = 'default_task')
    parser.add_argument('--values', '-v', action = "store", help = 'List of values to be extracted (comma separated)',
                        required = False, default = "")
    parser.add_argument('--identifier', '-i', action = "store",
                        help = 'Name of type for the values. Can be : mac, fti, aid, serial, nd. By default is mac',
                        required = False, default = "mac")
    parser.add_argument('--sources', action = "store",
                        help = 'Comma separated list of source to parse to extract the data. By default is tvdc',
                        required = False, default = "tvdc")
    parser.add_argument('--start_date', '-s', action = "store",
                        help = 'Start date (timestamp in s) for extraction (default 1 week ago)',
                        required = False, default = (time.time() - 604800), type = float)
    parser.add_argument('--end_date', '-e', action = "store",
                        help = 'End date (timestamp in s) for extraction (default now)',
                        required = False, default = time.time(), type = float)
    parser.add_argument('--workspace', '-w', action = "store",
                        help = 'Path of the workspace. If this option is used no other one will be use',
                        required = False, default = None)
    parser.add_argument('--rootdir', '-c', action = "store",
                        help = 'Path of the configuration directory (default:%s) % PORTAIL_BASE',
                        required = False, default = PORTAIL_BASE)

    args = parser.parse_args()

    confdir=os.path.join(args.rootdir,'conf')
    scriptdir=os.path.join(args.rootdir,'requests')

    if not os.path.isdir(args.rootdir):
        logger.error("The dir '%s' is not found" % args.rootdir)
        sys.exit(1)

    if args.workspace:
        logger.info("Set of the workspace to %s" % args.workspace)
        Interface.set_workspace(args.workspace,confdir)
        sys.exit(0)

    # No options => Show help
    if args.values == "":
        parser.print_help()
        sys.exit(0)

    if args.debug:
        ch.setLevel(logging.DEBUG)

    workspace = Interface.get_workspace(confdir)
    destination = os.path.join(workspace, args.taskname)

    logger.debug(u"Filtered %s: %s" % (args.identifier, args.values))
    logger.debug(u"Start date: %s" % (time.ctime(float(args.start_date))))
    logger.debug(u'End date: %s' % (time.ctime(float(args.end_date))))
    logger.debug("Workspace: %s" % workspace)
    logger.debug("Task output directory: %s" % destination)
    logger.debug("Root dir: %s" % args.rootdir)

    #

    if not workspace:
        logger.error("The workspace is not set !")
        logger.info("Please set the workspace with --workspace <path>")
        sys.exit(1)
    (rc, result) = Interface.extraction(args.start_date,
                                        args.end_date,
                                        path_script = scriptdir,
                                        values = args.values.split(','),
                                        type_field = args.identifier,
                                        sources = args.sources.split(','),
                                        path_destination = destination,
                                        dry_mode= args.dry
                                        )
    # logger.info('RESULT=======================================>\n%s %s' % (rc, result))
