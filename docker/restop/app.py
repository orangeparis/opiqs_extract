#!/usr/bin/python
# -*- coding: utf-8 -*-


from applications.opiqs.application import app,backend, start_server

app.config['DRY_MODE']= True
app.config['SECRET_KEY'] = 'you-will-never-guess'

app.config['WEB_URL_PREFIX']= "/"


app.config['EXTRACTOR_DIR']= "/opt/opiqs/"
app.config['EXTRACTOR_EXECUTABLE']= "/opt/opiqs/bin/extractor.py"
app.config['EXTRACTOR_RESULT_FILE']= "read_tvdc.csv"

app.config['WORKSPACE_ROOT']= app.config['EXTRACTOR_DIR'] + "workspace"


app.config['WHITELISTS']= {

    'default': ['MAC1','MAC2']

}

app.config['USERS']= [

    {
        'name': 'anonymus',
        'password': 'anonymus',
        'whitelist': 'default'
    },

    {
        'name': 'cocoon',
        'password': 'cocoon',
        'whitelist': 'default'
    },
    {
        'name': 'opiqs',
        'password': 'opiqs',
        'whitelist': 'default'
    }


]


backend.workspace_base=app.config['WORKSPACE_ROOT']

#backend.db.flushdb()

start_server(host="0.0.0.0", port=5001)

